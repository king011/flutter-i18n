# flutter-i18n

flutter i18n tools

這是用 golang 實現的一個 工具 用來 簡化 flutter i18n 工作

# 操作步驟
下面 命令 都需要在 flutter 項目 根路徑下執行

1. flutter i18n 依賴 dart package [flutter_localizations intl intl_translation] 來完成 工作 所以 將 這三個包加入到 flutter

    ```
    dependencies:
        flutter_localizations:
            sdk: flutter
        intl: ^0.15.7

    dev_dependencies:
        intl_translation: ^0.17.3
    ```

1. 執行 flutter-i18n init 初始化項目

    ```sh
    flutter-i18n init -l zh-TW -l zh-HK -l en-US
    ```

    上述 操作 創建了代碼 來 支持 zh-TW zh-HK en-US 三種語言 會在 lib/i18n 檔案夾中 創建 如下檔案

    * .gitignore 讓 git 不要把一些臨時數據加入版本庫
    * generated_local.json 後續 flutter-i18n 命令需要的 配置 檔案
    * generated_i18n.dart 創建一個 class S 和 function localeResolutionCallback
       * S.delegate 你需要 將其 註冊到 localizationsDelegates 中
       * S.of 你需要調用 此函數 來返回 Localizations.of 以便獲取 S 提供的 翻譯條目
       * localeResolutionCallback 通常應該把此函數 註冊到 系統中來解析 使用的 語言 環境 除非 你要自己實現 localeResolutionCallback
    * generated_resource.dart 創建了 mixin AppResource
        * 你應該把要翻譯的 東西都 寫在這裏
        * AppResource 提供了兩個 默認的 翻譯條目 你可以作爲參考
        * AppResource 被 class S with AppResource 所以 其方法 可以直接 被 S.of.XXX 來調用
    * intl_messages.arb intl_translation:extract_to_arb 創建的 arb 檔案
    * 多個 intl_messages_XXX.arb 這是 工具爲 指定的 語言環境 創建的 arb 檔案

1. 修改 generated_resource.dart 在 AppResource 中 添加好需要 翻譯的 內容

1. 執行 flutter-i18n arb
    1. 此命令會自動 將 AppResource 中待翻譯內容 提取到 intl_messages.arb 
    1. 由 intl_messages.arb 更新 各個 intl_messages_XXX.arb 檔案
    > intl_messages_XXX.arb 中 已經被翻譯的 內容不會被 覆蓋 會被 保留 所以 你可以 放心的使用 flutter-i18n arb 指令
    >
    > 但是 intl_messages.arb 中 被刪除的 翻譯條目 也會從 intl_messages_XXX.arb 中 被刪除
    >
    > (比如 之前有個 AppName AppHome 你 已經 完成了 翻譯  
    > 現在 intl_messages.arb 中 不存在 AppHome 執行 flutter-i18n arb 命令 會保留 已經翻譯的 AppName 結果 AppHome 則會被 徹底 移除)

1. 編輯 intl_messages_XXX.arb 完成 對各個 語言的 翻譯

1. 執行 `flutter-i18n dart` 由 arb 創建 dart 代碼

下面 是一個 註冊 語言支持 到 MaterialApp 的例子
```dart
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/foundation.dart';

// import flutter-i18n 創建的 檔案 class S
import 'i18n/generated_i18n.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        S.delegate, // 註冊 S.delegate
      ],
      // 註冊 語言 解析 函數
      localeResolutionCallback: localeResolutionCallback,
      // 調用  S.of(context).XXX 獲取 翻譯
      onGenerateTitle: (context) => S.of(context).appName,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
```

# 命令一覽

* bash-completion 指令 爲 linux 創建 bash-completion 腳本
* init 初始化 項目 並生成 dart 代碼 模板
* arb 更新 翻譯 條目
* check 檢查一個 arb 檔案中所有 未翻譯的 項目 並打印出來 
* dart 由翻譯條目 創建 dart 代碼

```sh
$ flutter-i18n -h
flutter i18n tools

Usage:
  flutter-i18n [flags]
  flutter-i18n [command]

Available Commands:
  arb             update arb files
  bash-completion create bash-completion scripts
  check           check arb,found not translate item
  dart            update dart from arb files
  help            Help about any command
  init            init intl_messages.arb and AppLocalizations.dart

Flags:
  -h, --help      help for flutter-i18n
  -v, --version   show version

Use "flutter-i18n [command] --help" for more information about a command.
```

# 代碼 模板

init 指令 會 創建一個 generated_i18n.dart 檔案 

可以 通過 -t 來指定 自己實現的 模板 

flutter-i18n 會加載 flutter-i18n 所在位置下的 flutter-i18n/模板名.template 作爲 模板實現


下面是 默認的 模板代碼

```
// tools auto create at {{.Now}}
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'messages_all.dart';
import 'generated_resource.dart';

const Map<String, String> _language = { {{range .Language}}
  '{{.Language}}': '{{.Country}}',{{end}}
};

const Set<String> _supported = { {{range .Locale}}
  '{{.}}',{{end}}
};

Locale localeResolutionCallback(
  Locale locale,
  Iterable<Locale> supportedLocales,
) {
  String name;
  if (locale.countryCode.isEmpty) {
    name = locale.languageCode;
  } else {
    name = "${locale.languageCode}_${locale.countryCode}";
  }
  if (_supported.contains(name)) {
    return locale;
  }
  var language = locale.languageCode;
  if (_language.containsKey(language)) {
    var country = _language[language];
    if (country.isEmpty) {
      return Locale(language);
    } else {
      return Locale(language, country);
    }
  }
  return const Locale("en", "US");
}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<S> {
  const GeneratedLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => _language.containsKey(locale.languageCode);

  @override
  Future<S> load(Locale locale) => S.load(locale);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;
}

class S with AppResource {
  static const GeneratedLocalizationsDelegate delegate =
      GeneratedLocalizationsDelegate();
  static S of(BuildContext context) => Localizations.of<S>(context, S);

  static Future<S> load(Locale locale) {
    final name = locale.countryCode.isEmpty
        ? locale.languageCode
        : "${locale.languageCode}_${locale.countryCode}";
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return S();
    });
  }
}
```