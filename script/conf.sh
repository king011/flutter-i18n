Target="flutter-i18n"
Docker="flutter-i18n"
Dir=$(cd "$(dirname $BASH_SOURCE)/.." && pwd)
Version="v1.0.9"
Platforms=(
    darwin/amd64
    windows/amd64
    linux/arm
    linux/amd64
)