package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/cmd/internal/completion"
)

func init() {
	var output string
	cmd := &cobra.Command{
		Use:   "bash-completion",
		Short: "create bash-completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			if output == "" {
				e := completion.Write(os.Stdout)
				if e != nil {
					log.Fatalln(e)
				}
			} else {
				f, e := os.Create(output)
				if e != nil {
					log.Fatalln(e)
				}
				e = completion.Write(f)
				f.Close()
				if e != nil {
					log.Fatalln(e)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&output,
		"output", "o",
		"",
		"output file,if empty output stdout",
	)
	rootCmd.AddCommand(cmd)
}
