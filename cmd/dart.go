package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/cmd/internal/utils"
)

func init() {
	var i18n string
	var files, directory []string
	cmd := &cobra.Command{
		Use:   "dart",
		Short: "update dart from arb files",
		Run: func(cmd *cobra.Command, args []string) {
			f, e := os.Open("pubspec.yaml")
			if e != nil {
				log.Fatalln(e)
			}
			f.Close()

			e = utils.Dart(i18n, directory, files)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()

	flags.StringVarP(&i18n,
		"i18n", "i",
		"lib/i18n",
		"i18n directory",
	)
	flags.StringArrayVarP(&files,
		"file", "f",
		nil,
		"dart source files",
	)
	flags.StringArrayVarP(&directory,
		"directory", "d",
		[]string{"source"},
		"dart source directory",
	)
	rootCmd.AddCommand(cmd)
}
