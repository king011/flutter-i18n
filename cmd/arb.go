package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/cmd/internal/utils"
)

func init() {
	var i18n string
	var files, directory []string
	var skip bool
	cmd := &cobra.Command{
		Use:   "arb",
		Short: "update arb files",
		Run: func(cmd *cobra.Command, args []string) {
			f, e := os.Open("pubspec.yaml")
			if e != nil {
				log.Fatalln(e)
			}
			f.Close()
			e = utils.ARB(i18n, skip, directory, files)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()

	flags.StringVarP(&i18n,
		"i18n", "i",
		"lib/i18n",
		"i18n directory",
	)
	flags.StringArrayVarP(&files,
		"file", "f",
		nil,
		"dart source files",
	)
	flags.StringArrayVarP(&directory,
		"directory", "d",
		[]string{"source"},
		"dart source directory",
	)
	flags.BoolVarP(&skip,
		"skip", "s",
		false,
		"skip run command intl_translation:extract_to_arb",
	)
	rootCmd.AddCommand(cmd)
}
