package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// ARB .
func ARB(i18n string, skip bool, directorys, files []string) (e error) {
	dir, e := filepath.Abs(i18n)
	if e != nil {
		return
	}
	b, e := ioutil.ReadFile(dir + _LocaleFile)
	if e != nil {
		return
	}
	var locale []string
	e = json.Unmarshal(b, &locale)
	if e != nil {
		return
	}
	locale, e = getLocale(locale)
	if e != nil {
		return
	}
	e = generateARB(dir, locale, skip, directorys, files)
	return
}

func extractARB(dir, source string) (e error) {
	cmd := exec.Command(
		"flutter",
		"pub", "pub", "run",
		"intl_translation:extract_to_arb",
		"--output-dir="+dir,
		"--sources-list-file="+source,
	)
	fmt.Println(strings.Join(cmd.Args, " "))
	e = cmd.Run()
	if e != nil {
		return
	}
	return
}
func generateSourceList(dir, source string, directorys, files []string) (e error) {
	f, e := os.Create(source)
	if e != nil {
		return e
	}
	defer f.Close()
	str := filepath.Clean(dir + _Resource)
	_, e = f.WriteString(str + "\n")
	if e != nil {
		return e
	}
	keys := make(map[string]bool)
	keys[str] = true
	for _, directory := range directorys {
		if filepath.IsAbs(directory) {
			directory = filepath.Clean(directory)
		} else {
			directory = filepath.Clean(dir + "/" + directory)
		}
		e = filepath.Walk(directory, func(path string, info os.FileInfo, err error) (e error) {
			if e != nil || info == nil {
				return
			}
			if info.IsDir() {
				return
			}
			if !strings.HasSuffix(strings.ToLower(info.Name()), ".dart") {
				return
			}
			if keys[path] {
				return
			}
			keys[path] = true
			_, e = f.WriteString(path + "\n")
			return
		})
	}
	if e != nil {
		return
	}
	for _, str := range files {
		if filepath.IsAbs(str) {
			str = filepath.Clean(str)
		} else {
			str = filepath.Clean(dir + "/" + str)
		}
		if keys[str] {
			continue
		}
		keys[str] = true
		_, e = f.WriteString(str + "\n")
		if e != nil {
			return
		}
	}
	return
}
func generateARB(dir string, locale []string, skip bool, directorys, files []string) (e error) {
	// 執行 intl_translation:extract_to_arb
	if !skip {
		source := filepath.Clean(dir + _ResourceList)
		e = generateSourceList(dir, source, directorys, files)
		if e != nil {
			return e
		}
		e = extractARB(dir, source)
		if e != nil {
			return
		}
	}

	// 解析到 內存
	var obj Object
	e = obj.fromFile(dir+"/intl_messages.arb", "en")
	if e != nil {
		return
	}
	obj.fillNil()
	// 排序
	obj.sort()
	// 創建 特例
	for i := 0; i < len(locale); i++ {
		e = generateARBLocale(&obj, dir, locale[i])
		if e != nil {
			return
		}
	}
	return
}
func generateARBLocale(obj *Object, dir, locale string) (e error) {
	filename := dir + "/intl_messages_" + locale + ".arb"
	_, e = os.Stat(filename)
	if e == nil {
		e = mergeARBLocale(obj, filename, locale)
	} else {
		if os.IsNotExist(e) {
			e = copyARBLocale(obj, filename, locale)
		}
	}
	return
}
func mergeARBLocale(obj *Object, filename, locale string) (e error) {
	var dst Object
	e = dst.fromFile(filename, locale)
	if e != nil {
		return
	}
	// 刪除 已經移除的 屬性
	keys := make(map[string]bool)
	for i := 0; i < len(obj.Attribute); i++ {
		item := &obj.Attribute[i]
		keys[item.Name] = true
	}
	for i := 0; i < len(dst.Attribute); i++ {
		item := &dst.Attribute[i]
		k := item.Name
		item.Skip = !keys[k]
	}
	// 增加 沒有的 屬性
	keys = make(map[string]bool)
	for i := 0; i < len(dst.Attribute); i++ {
		item := &dst.Attribute[i]
		keys[item.Name] = true
	}
	for i := 0; i < len(obj.Attribute); i++ {
		item := &obj.Attribute[i]
		if keys[item.Name] {
			continue
		}
		dst.Attribute = append(dst.Attribute, *item)
	}

	// 刪除 已經移除的 條目
	keys = make(map[string]bool)
	for i := 0; i < len(obj.Target); i++ {
		item := &obj.Target[i].Value
		keys[item.Name] = true
	}
	for i := 0; i < len(dst.Target); i++ {
		item := &dst.Target[i].Value
		k := item.Name
		item.Skip = !keys[k]
	}
	// 增加 沒有的 條目
	keys = make(map[string]bool)
	for i := 0; i < len(dst.Target); i++ {
		item := &dst.Target[i].Value
		keys[item.Name] = true
	}
	for i := 0; i < len(obj.Target); i++ {
		item := &obj.Target[i].Value
		if keys[item.Name] {
			continue
		}
		dst.Target = append(dst.Target, obj.Target[i])
	}

	// 寫入到 檔案
	f, e := os.Create(filename)
	if e != nil {
		return
	}
	_, e = dst.WriteTo(f)
	f.Close()
	return
}
func copyARBLocale(obj *Object, filename, locale string) (e error) {
	clone := obj.Clone()
	clone.Locale = locale
	f, e := os.Create(filename)
	if e != nil {
		return
	}
	_, e = clone.WriteTo(f)
	f.Close()
	return
}
