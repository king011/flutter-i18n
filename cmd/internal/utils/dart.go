package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

// Dart .
func Dart(i18n string, directory, files []string) (e error) {
	dir, e := filepath.Abs(i18n)
	if e != nil {
		return
	}
	b, e := ioutil.ReadFile(dir + _LocaleFile)
	if e != nil {
		return
	}
	var locale []string
	e = json.Unmarshal(b, &locale)
	if e != nil {
		return
	}
	locale, e = getLocale(locale)
	if e != nil {
		return
	}

	for i, str := range locale {
		locale[i] = dir + "/intl_messages_" + str + ".arb"
	}
	e = extractDart(dir, directory, files, locale)
	return
}
func extractDart(dir string, directorys, files, src []string) (e error) {
	source := filepath.Clean(dir + _ResourceList)
	e = generateSourceList(dir, source, directorys, files)
	if e != nil {
		return e
	}
	translation := filepath.Clean(dir + _TranslationList)
	f, e := os.Create(translation)
	if e != nil {
		return e
	}
	for _, str := range src {
		_, e = f.WriteString(str + "\n")
		if e != nil {
			f.Close()
			return e
		}
	}
	f.Close()

	cmd := exec.Command(
		"flutter",
		"pub", "pub", "run",
		"intl_translation:generate_from_arb",
		"--output-dir="+dir,
		"--sources-list-file="+source,
		"--translations-list-file="+translation,
		"--no-use-deferred-loading",
	)
	fmt.Println(strings.Join(cmd.Args, " "))
	e = cmd.Run()
	if e != nil {
		return
	}
	return
}
