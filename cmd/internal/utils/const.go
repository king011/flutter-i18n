package utils

const _LocaleFile = "/generated_local.json"
const _DartFile = "/generated_i18n.dart"
const _Resource = "/generated_resource.dart"
const _ResourceList = "/source.list"
const _TranslationList = "/translation.list"
const _Gitignore = "/.gitignore"
