package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/king011/king-go/os/fileperm"
)

func getLocale(locale []string) ([]string, error) {
	if len(locale) == 0 {
		return nil, errors.New("locale empty")
	}
	arrs := make([]string, 0, len(locale))
	keys := make(map[string]bool)
	for _, str := range locale {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		str = strings.Replace(str, "-", "_", -1)
		if keys[str] {
			continue
		}
		keys[str] = true
		arrs = append(arrs, str)
	}
	locale = arrs
	if len(locale) == 0 {
		return nil, errors.New("locale empty")
	}
	return locale, nil
}
func generateTemplate(templateName, dir string, locale []string) (e error) {
	t, e := getTemplate(templateName)
	if e != nil {
		return
	}

	f, e := os.Create(dir + _DartFile)
	if e != nil {
		return
	}
	e = t.Execute(f, &struct {
		Now      string
		Locale   []string
		Language []Locale
	}{
		Now:      fmt.Sprint(time.Now().Format("2006-01-02 15:04:05")),
		Locale:   locale,
		Language: getLanguage(locale),
	})
	f.Close()

	return
}
func generateJSON(dir string, locale []string) (e error) {
	b, e := json.MarshalIndent(locale, "", "\t")
	if e != nil {
		return
	}
	e = ioutil.WriteFile(dir+_LocaleFile, b, fileperm.File)
	return
}

func generateResource(dir string) (e error) {
	filename := dir + _Resource
	_, e = os.Stat(filename)
	if e == nil || !os.IsNotExist(e) {
		return
	}
	e = ioutil.WriteFile(filename,
		[]byte(Resource),
		fileperm.File,
	)
	return
}

// Init .
func Init(templateName, i18n string, locale []string) (e error) {
	// 返回 語言
	locale, e = getLocale(locale)
	if e != nil {
		return
	}

	// 返回 輸出 目錄
	dir, e := filepath.Abs(i18n)
	if e != nil {
		return
	}
	os.MkdirAll(dir, fileperm.Directory)

	// 創建 代碼 模板
	e = generateTemplate(templateName, dir, locale)
	if e != nil {
		return
	}
	// 創建 配置
	e = generateJSON(dir, locale)
	if e != nil {
		return
	}
	// 創建 資源 dartk
	e = generateResource(dir)
	if e != nil {
		return
	}

	// 創建 .gitignore
	e = generateGitignore(dir + _Gitignore)
	if e != nil {
		return
	}
	// 創建 arb 檔案
	e = generateARB(dir, locale, false, nil, nil)
	return
}
func generateGitignore(filename string) (e error) {
	_, e = os.Stat(filename)
	if e == nil || !os.IsNotExist(e) {
		return
	}

	e = ioutil.WriteFile(filename, []byte(Gitignore), fileperm.File)
	return
}

// Locale .
type Locale struct {
	Language string
	Country  string
}

func getLanguage(locale []string) (language []Locale) {
	keys := make(map[string]bool)
	var languageName, countryName string
	for i := 0; i < len(locale); i++ {
		str := locale[i]
		pos := strings.Index(str, "_")
		if pos == -1 {
			languageName = str
			countryName = ""
		} else {
			languageName = str[:pos]
			countryName = str[pos+1:]
		}
		if keys[languageName] {
			continue
		}
		keys[languageName] = true
		language = append(language, Locale{
			Language: languageName,
			Country:  countryName,
		})
	}
	return
}
