package utils

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

// Check .
func Check(filename string) (e error) {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	var m map[string]json.RawMessage
	e = json.Unmarshal(b, &m)
	if e != nil {
		return
	}
	var str string
	for k, v := range m {
		if strings.HasPrefix(k, "@") {
			continue
		}
		e = json.Unmarshal(v, &str)
		if e != nil {
			e = fmt.Errorf("[%v] %v", k, e)
			continue
		}
		if str == "" {
			fmt.Println("found", k)
		}
	}
	return
}
