package utils

import (
	"html/template"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
)

// Gitignore .gitignore
const Gitignore = `/intl_messages.arb
/messages_*.dart
/source.list
/translation.list
`

// Resource dart code
const Resource = `import 'package:intl/intl.dart';

mixin AppResource {
  String get appName => Intl.message('App Name');
  String idae(name) => Intl.message(
        "$name is an idae",
        name: "idae",
        args: [name],
        desc: "example for message input args",
        examples: const {
          "name": "cerberus",
        },
      );
}
`

// Template dart code
const Template = `// tools auto create at {{.Now}}
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'messages_all.dart';
import 'generated_resource.dart';

const Map<String, String> _language = { {{range .Language}}
  '{{.Language}}': '{{.Country}}',{{end}}
};

const Set<String> _supported = { {{range .Locale}}
  '{{.}}',{{end}}
};

Locale localeResolutionCallback(
  Locale locale,
  Iterable<Locale> supportedLocales,
) {
  String name;
  if (locale.countryCode.isEmpty) {
    name = locale.languageCode;
  } else {
    name = "${locale.languageCode}_${locale.countryCode}";
  }
  if (_supported.contains(name)) {
    return locale;
  }
  var language = locale.languageCode;
  if (_language.containsKey(language)) {
    var country = _language[language];
    if (country.isEmpty) {
      return Locale(language);
    } else {
      return Locale(language, country);
    }
  }
  return const Locale("en", "US");
}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<S> {
  const GeneratedLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) => _language.containsKey(locale.languageCode);

  @override
  Future<S> load(Locale locale) => S.load(locale);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;
}

class S with AppResource {
  static const GeneratedLocalizationsDelegate delegate =
      GeneratedLocalizationsDelegate();
  static S of(BuildContext context) => Localizations.of<S>(context, S);

  static Future<S> load(Locale locale) {
    final name = locale.countryCode.isEmpty
        ? locale.languageCode
        : "${locale.languageCode}_${locale.countryCode}";
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      return S();
    });
  }
}
`

func loadTemplate(name string) (str string, e error) {
	path, e := exec.LookPath(os.Args[0])
	if e != nil {
		return
	}
	path, e = filepath.Abs(path)
	if e != nil {
		return
	}
	path = filepath.Clean(filepath.Dir(path) + "/flutter-i18n/" + name + ".template")
	b, e := ioutil.ReadFile(path)
	if e != nil {
		return
	}
	str = string(b)
	return
}
func getTemplate(name string) (t *template.Template, e error) {
	var str string
	if name == "" {
		str = Template
	} else {
		str, e = loadTemplate(name)
		if e != nil {
			return
		}
	}
	t = template.New("init")
	t, e = t.Parse(str)
	if e != nil {
		return
	}
	return
}
