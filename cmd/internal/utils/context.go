package utils

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"sort"
	"strings"
)

// Item 每個節點
type Item struct {
	Name  string
	Value json.RawMessage
	// 如果爲 true 不寫法到 檔案
	Skip bool
}

// SortItem .
type SortItem []Item

// Len is the number of elements in the collection.
func (arrs SortItem) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs SortItem) Less(i, j int) bool {
	return arrs[i].Name < arrs[j].Name
}

// Swap swaps the elements with indexes i and j.
func (arrs SortItem) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}

// CopyFrom .
func (i *Item) CopyFrom(clone *Item) {
	i.Name = clone.Name
	i.Value = clone.Value
}

// Target 一個需要翻譯的 條目
type Target struct {
	Value Item
	// @ 註釋
	Note Item
}

// CopyFrom .
func (t *Target) CopyFrom(clone *Target) {
	t.Value.CopyFrom(&clone.Value)
	t.Note.CopyFrom(&clone.Note)
}

// SortTarget .
type SortTarget []Target

// Len is the number of elements in the collection.
func (arrs SortTarget) Len() int {
	return len(arrs)
}

// Less reports whether the element with
// index i should sort before the element with index j.
func (arrs SortTarget) Less(i, j int) bool {
	return arrs[i].Value.Name < arrs[j].Value.Name
}

// Swap swaps the elements with indexes i and j.
func (arrs SortTarget) Swap(i, j int) {
	arrs[i], arrs[j] = arrs[j], arrs[i]
}

// Object 一個 arb 檔案的內存映射
type Object struct {
	// @@
	Attribute []Item

	Target []Target

	// 目標語言
	// @@locale
	Locale string
	Last   string
}

func (o *Object) sort() {
	sort.Stable(SortItem(o.Attribute))
	sort.Stable(SortTarget(o.Target))
}
func (o *Object) fillNil() {
	for i := 0; i < len(o.Target); i++ {
		o.Target[i].Value.Value = json.RawMessage(`""`)
	}
}
func (o *Object) fromFile(filename, locale string) (e error) {
	b, e := ioutil.ReadFile(filename)
	if e != nil {
		return
	}
	var m map[string]json.RawMessage
	e = json.Unmarshal(b, &m)
	if e != nil {
		return
	}

	// 需要 target
	var target []Target
	var last string
	for k, v := range m {
		if k == "@@last_modified" {
			var str string
			e = json.Unmarshal(v, &str)
			if e != nil {
				return
			}
			last = str
			delete(m, k)
			continue
		}
		if k == "@@locale" {
			var str string
			e = json.Unmarshal(v, &str)
			if e != nil {
				return
			}
			if str != locale {
				e = fmt.Errorf("locale not match,hope %v,but @@locale=%v", locale, str)
				return
			}
			delete(m, k)
			continue
		}
		if strings.HasPrefix(k, "@") {
			continue
		}
		target = append(target, Target{
			Value: Item{
				Name:  k,
				Value: v,
			},
		})
		delete(m, k)
	}
	// 尋找 目標
	for i := 0; i < len(target); i++ {
		k := "@" + target[i].Value.Name
		if v, ok := m[k]; ok {
			target[i].Note = Item{
				Name:  k,
				Value: v,
			}
			delete(m, k)
		}
	}
	// 屬性
	var attrs []Item
	for k, v := range m {
		attrs = append(attrs, Item{
			Name:  k,
			Value: v,
		})
	}

	o.Target = target
	o.Attribute = attrs
	o.Locale = locale
	o.Last = last
	return
}

// Clone 創建 副本
func (o *Object) Clone() (clone *Object) {
	clone = &Object{
		Locale: o.Locale,
		Last:   o.Last,
	}
	if len(o.Attribute) != 0 {
		clone.Attribute = make([]Item, len(o.Attribute))
		for i := 0; i < len(o.Attribute); i++ {
			clone.Attribute[i].CopyFrom(&o.Attribute[i])
		}
	}
	if len(o.Target) != 0 {
		clone.Target = make([]Target, len(o.Target))
		for i := 0; i < len(o.Target); i++ {
			clone.Target[i].CopyFrom(&o.Target[i])
		}
	}
	return
}

// WriteTo .
func (o *Object) WriteTo(w io.Writer) (sum int64, e error) {
	bw := bufio.NewWriter(w)
	defer bw.Flush()
	var n int
	n, e = bw.WriteString("{")
	if e != nil {
		return
	}
	sum += int64(n)

	// 寫入 基本 屬性
	if o.Last != "" {
		e = o.writeKey(bw,
			"@@last_modified", o.Last,
			&sum, true,
		)
		if e != nil {
			return
		}
	}
	e = o.writeKey(bw,
		"@@locale", o.Locale,
		&sum, false,
	)
	if e != nil {
		return
	}
	// 寫入 其它屬性
	for i := 0; i < len(o.Attribute); i++ {
		if o.Attribute[i].Skip {
			continue
		}
		e = o.writeRaw(bw,
			o.Attribute[i].Name, o.Attribute[i].Value,
			&sum, false,
		)
		if e != nil {
			return
		}
	}
	// 寫入 翻譯 條目
	for i := 0; i < len(o.Target); i++ {
		item := &o.Target[i].Value
		if item.Skip {
			continue
		}
		e = o.writeRaw(bw,
			item.Name, item.Value,
			&sum, false,
		)
		if e != nil {
			return
		}

		item = &o.Target[i].Note
		if item.Name == "" {
			continue
		}
		e = o.writeRaw(bw,
			item.Name, item.Value,
			&sum, false,
		)
		if e != nil {
			return
		}
	}

	n, e = bw.WriteString("\n}")
	if e != nil {
		return
	}
	sum += int64(n)
	return
}
func (o *Object) writeKey(w *bufio.Writer,
	key string, val interface{},
	sum *int64,
	first bool,
) (e error) {
	name, e := json.Marshal(key)
	if e != nil {
		return
	}
	b, e := json.Marshal(val)
	if e != nil {
		return
	}
	var n int
	if first {
		n, e = w.WriteString(fmt.Sprintf("\n  %s: %s", name, b))
	} else {
		n, e = w.WriteString(fmt.Sprintf(",\n  %s: %s", name, b))
	}
	if n != 0 {
		*sum += int64(n)
	}
	return
}
func (o *Object) writeRaw(w *bufio.Writer,
	key string, val json.RawMessage,
	sum *int64,
	first bool,
) (e error) {
	name, e := json.Marshal(key)
	if e != nil {
		return
	}
	var n int
	if first {
		n, e = w.WriteString(fmt.Sprintf("\n  %s: %s", name, val))
	} else {
		n, e = w.WriteString(fmt.Sprintf(",\n  %s: %s", name, val))
	}
	if n != 0 {
		*sum += int64(n)
	}
	return
}
