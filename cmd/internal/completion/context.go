package completion

import (
	"fmt"
	"io"
	"text/template"
	"time"
)

// Write .
func Write(w io.Writer) (e error) {
	t := template.New("bash")
	t, e = t.Parse(Template)
	if e != nil {
		return
	}
	e = t.Execute(w, &struct {
		Now string
	}{
		Now: fmt.Sprint(time.Now().Format("2006-01-02 15:04:05")),
	})
	return
}
