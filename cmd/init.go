package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/cmd/internal/utils"
)

func init() {
	var template, i18n string
	var locale []string
	cmd := &cobra.Command{
		Use:   "init",
		Short: "init intl_messages.arb and AppLocalizations.dart",
		Run: func(cmd *cobra.Command, args []string) {
			f, e := os.Open("pubspec.yaml")
			if e != nil {
				log.Fatalln(e)
			}
			f.Close()
			e = utils.Init(template, i18n, locale)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&template,
		"template", "t",
		"",
		"template name",
	)
	flags.StringVarP(&i18n,
		"i18n", "i",
		"lib/i18n",
		"i18n directory",
	)
	flags.StringSliceVarP(&locale,
		"locale", "l",
		nil,
		"support locale name",
	)
	rootCmd.AddCommand(cmd)
}
