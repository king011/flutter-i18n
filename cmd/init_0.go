package cmd

import (
	"fmt"
	"os"
	"runtime"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/version"
)

const (
	// App 程式名
	App = "flutter-i18n"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "flutter i18n tools",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(runtime.GOOS, runtime.GOARCH, runtime.Version())
			fmt.Println(version.Version)
		} else {
			fmt.Println(App)
			fmt.Println(runtime.GOOS, runtime.GOARCH, runtime.Version())
			fmt.Println(version.Version)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
}

// Execute 執行命令
func Execute() error {
	return rootCmd.Execute()
}
func abort() {
	os.Exit(1)
}
