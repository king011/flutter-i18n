package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/flutter-i18n/cmd/internal/utils"
)

func init() {
	var filename string
	cmd := &cobra.Command{
		Use:   "check",
		Short: "check arb,found not translate item",
		Run: func(cmd *cobra.Command, args []string) {
			f, e := os.Open("pubspec.yaml")
			if e != nil {
				log.Fatalln(e)
			}
			f.Close()

			e = utils.Check(filename)
			if e != nil {
				log.Fatalln(e)
			}
		},
	}
	flags := cmd.Flags()

	flags.StringVarP(&filename,
		"filename", "f",
		"",
		"arb filename",
	)
	rootCmd.AddCommand(cmd)
}
