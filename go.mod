module gitlab.com/king011/flutter-i18n

go 1.16

require (
	github.com/spf13/cobra v1.2.1
	gitlab.com/king011/king-go v0.0.10
)
