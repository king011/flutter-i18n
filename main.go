package main

import (
	"log"

	"gitlab.com/king011/flutter-i18n/cmd"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
